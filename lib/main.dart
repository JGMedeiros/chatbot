import 'package:flutter/material.dart';
import 'package:chatbot_pormade/views/login_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MobX Tutorial',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.lightGreen,
        cursorColor: Colors.lightGreen,
        backgroundColor: Colors.white,
        scaffoldBackgroundColor: Colors.lightGreen,
        iconTheme: IconThemeData(color: Colors.lightGreen),
      ),
      home: LoginScreen(),
    );
  }
}
