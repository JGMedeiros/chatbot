import 'package:flutter/material.dart';

import 'custom_text_field.dart';

class TextComposer extends StatefulWidget {
  @override
  _TextComposerState createState() => _TextComposerState();
}

class _TextComposerState extends State<TextComposer> {

  bool _isComposing = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(8),
      child: Row(
        children: <Widget>[
          Expanded(child:  CustomTextField(
            hint: 'Digite sua Mensagem',
            prefix: Icon(Icons.message),
            textInputType: TextInputType.text,

          onChanged: (text){
            setState(() {
              _isComposing = text.isNotEmpty;
            });
          },
          ),
          ),
          IconButton(
              icon: Icon(Icons.send),
              onPressed: _isComposing ?(){

          } : null
          )
        ],
      )
    );
  }
}
