import 'dart:convert';
import 'dart:io';
import 'package:chatbot_pormade/models/usuario.dart';
import 'package:http/http.dart' as http;

class LoginApi {
  static Future<Usuario> login(String login, String password) async {
    var url = 'https://aline.digitaly.tech/api/login';
    var header = {"Content-Type": "application/json"};

    Map params = {"login": login, "password": password};

    var usuario;

    var _body = json.encode(params);

    var response = await http.post(url, headers: header, body: _body);

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    Map mapResponse = json.decode(response.body);

    if (response.statusCode == 200) {
      usuario = Usuario.fromJson(mapResponse);
    } else {
      usuario = null;
    }
    return usuario;
  }
}
