class Usuario {
  String accessToken;
  String name;

  Usuario({this.accessToken, this.name});

  Usuario.fromJson(Map<String, dynamic> json) {
    accessToken = json['accessToken'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accessToken'] = this.accessToken;
    data['name'] = this.name;
    return data;
  }

  String toString(){

    return 'Usuario(token: $accessToken)';

  }

}