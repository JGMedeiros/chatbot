import 'dart:convert';
import 'dart:io';
import 'package:chatbot_pormade/models/chat.dart';
import 'package:http/http.dart' as http;

class ChatApi{

  static Future<Chat> chat(String message) async{
   var url = 'https://aline.digitaly.tech/api/conversation/message';
   var header = {"Content-Type": "application/json"};

   Map params = {"message": message};

   var chat;

   var _body = json.encode(params);

   var response = await http.post(url, headers: header, body: _body);

   print('Response status: ${response.statusCode}');
   print('Response body: ${response.body}');

   Map mapResponse = json.decode(response.body);

   if (response.statusCode == 200) {
     chat = Chat.fromJson(mapResponse);
   } else {
     chat = null;
   }
   return chat;
  }
}