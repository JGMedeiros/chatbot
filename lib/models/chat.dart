class Chat {
  List<Intents> intents;
  List<Entities> entities;
  Input input;
  Output output;
  Context context;
  bool alternateIntents;
  String userId;

  Chat(
      {this.intents,
        this.entities,
        this.input,
        this.output,
        this.context,
        this.alternateIntents,
        this.userId});

  Chat.fromJson(Map<String, dynamic> json) {
    if (json['intents'] != null) {
      intents = new List<Intents>();
      json['intents'].forEach((v) {
        intents.add(new Intents.fromJson(v));
      });
    }
    if (json['entities'] != null) {
      entities = new List<Entities>();
      json['entities'].forEach((v) {
        entities.add(new Entities.fromJson(v));
      });
    }
    input = json['input'] != null ? new Input.fromJson(json['input']) : null;
    output =
    json['output'] != null ? new Output.fromJson(json['output']) : null;
    context =
    json['context'] != null ? new Context.fromJson(json['context']) : null;
    alternateIntents = json['alternate_intents'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.intents != null) {
      data['intents'] = this.intents.map((v) => v.toJson()).toList();
    }
    if (this.entities != null) {
      data['entities'] = this.entities.map((v) => v.toJson()).toList();
    }
    if (this.input != null) {
      data['input'] = this.input.toJson();
    }
    if (this.output != null) {
      data['output'] = this.output.toJson();
    }
    if (this.context != null) {
      data['context'] = this.context.toJson();
    }
    data['alternate_intents'] = this.alternateIntents;
    data['user_id'] = this.userId;
    return data;
  }
}

class Intents {
  String intent;
  int confidence;

  Intents({this.intent, this.confidence});

  Intents.fromJson(Map<String, dynamic> json) {
    intent = json['intent'];
    confidence = json['confidence'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['intent'] = this.intent;
    data['confidence'] = this.confidence;
    return data;
  }
}

class Entities {
  String entity;
  List<int> location;
  String value;
  int confidence;
  Metadata metadata;
  Interpretation interpretation;

  Entities(
      {this.entity,
        this.location,
        this.value,
        this.confidence,
        this.metadata,
        this.interpretation});

  Entities.fromJson(Map<String, dynamic> json) {
    entity = json['entity'];
    location = json['location'].cast<int>();
    value = json['value'];
    confidence = json['confidence'];
    metadata = json['metadata'] != null
        ? new Metadata.fromJson(json['metadata'])
        : null;
    interpretation = json['interpretation'] != null
        ? new Interpretation.fromJson(json['interpretation'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['entity'] = this.entity;
    data['location'] = this.location;
    data['value'] = this.value;
    data['confidence'] = this.confidence;
    if (this.metadata != null) {
      data['metadata'] = this.metadata.toJson();
    }
    if (this.interpretation != null) {
      data['interpretation'] = this.interpretation.toJson();
    }
    return data;
  }
}

class Metadata {
  String calendarType;
  int numericValue;

  Metadata({this.calendarType, this.numericValue});

  Metadata.fromJson(Map<String, dynamic> json) {
    calendarType = json['calendar_type'];
    numericValue = json['numeric_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['calendar_type'] = this.calendarType;
    data['numeric_value'] = this.numericValue;
    return data;
  }
}

class Interpretation {
  int numericValue;
  String calendarType;
  String subtype;

  Interpretation({this.numericValue, this.calendarType, this.subtype});

  Interpretation.fromJson(Map<String, dynamic> json) {
    numericValue = json['numeric_value'];
    calendarType = json['calendar_type'];
    subtype = json['subtype'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['numeric_value'] = this.numericValue;
    data['calendar_type'] = this.calendarType;
    data['subtype'] = this.subtype;
    return data;
  }
}

class Input {
  String text;

  Input({this.text});

  Input.fromJson(Map<String, dynamic> json) {
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['text'] = this.text;
    return data;
  }
}

class Output {
  List<Generic> generic;
  List<String> text;
  List<String> nodesVisited;
  List<Null> logMessages;

  Output({this.generic, this.text, this.nodesVisited, this.logMessages});

  Output.fromJson(Map<String, dynamic> json) {
    if (json['generic'] != null) {
      generic = new List<Generic>();
      json['generic'].forEach((v) {
        generic.add(new Generic.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.generic != null) {
      data['generic'] = this.generic.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Generic {
  String responseType;
  String text;
  String source;

  Generic({this.responseType, this.text, this.source});

  Generic.fromJson(Map<String, dynamic> json) {
    responseType = json['response_type'];
    text = json['text'];
    source = json['source'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response_type'] = this.responseType;
    data['text'] = this.text;
    data['source'] = this.source;
    return data;
  }
}

class Context {
  String conversationId;
  System system;
  bool isResponse;
  Metadata metadata;

  Context({this.conversationId, this.system, this.isResponse, this.metadata});

  Context.fromJson(Map<String, dynamic> json) {
    conversationId = json['conversation_id'];
    system =
    json['system'] != null ? new System.fromJson(json['system']) : null;
    isResponse = json['isResponse'];
    metadata = json['metadata'] != null
        ? new Metadata.fromJson(json['metadata'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['conversation_id'] = this.conversationId;
    if (this.system != null) {
      data['system'] = this.system.toJson();
    }
    data['isResponse'] = this.isResponse;
    if (this.metadata != null) {
      data['metadata'] = this.metadata.toJson();
    }
    return data;
  }
}

class System {
  bool initialized;
  List<DialogStack> dialogStack;
  int dialogTurnCounter;
  int dialogRequestCounter;
  NodeOutputMap nNodeOutputMap;
  String lastBranchNode;
  bool branchExited;
  String branchExitedReason;

  System(
      {this.initialized,
        this.dialogStack,
        this.dialogTurnCounter,
        this.dialogRequestCounter,
        this.nNodeOutputMap,
        this.lastBranchNode,
        this.branchExited,
        this.branchExitedReason});

  System.fromJson(Map<String, dynamic> json) {
    initialized = json['initialized'];
    if (json['dialog_stack'] != null) {
      dialogStack = new List<DialogStack>();
      json['dialog_stack'].forEach((v) {
        dialogStack.add(new DialogStack.fromJson(v));
      });
    }
    dialogTurnCounter = json['dialog_turn_counter'];
    dialogRequestCounter = json['dialog_request_counter'];
    nNodeOutputMap = json['_node_output_map'] != null
        ? new NodeOutputMap.fromJson(json['_node_output_map'])
        : null;
    lastBranchNode = json['last_branch_node'];
    branchExited = json['branch_exited'];
    branchExitedReason = json['branch_exited_reason'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['initialized'] = this.initialized;
    if (this.dialogStack != null) {
      data['dialog_stack'] = this.dialogStack.map((v) => v.toJson()).toList();
    }
    data['dialog_turn_counter'] = this.dialogTurnCounter;
    data['dialog_request_counter'] = this.dialogRequestCounter;
    if (this.nNodeOutputMap != null) {
      data['_node_output_map'] = this.nNodeOutputMap.toJson();
    }
    data['last_branch_node'] = this.lastBranchNode;
    data['branch_exited'] = this.branchExited;
    data['branch_exited_reason'] = this.branchExitedReason;
    return data;
  }
}

class DialogStack {
  String dialogNode;

  DialogStack({this.dialogNode});

  DialogStack.fromJson(Map<String, dynamic> json) {
    dialogNode = json['dialog_node'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dialog_node'] = this.dialogNode;
    return data;
  }
}

class NodeOutputMap {
  Node21611932303615 node21611932303615;

  NodeOutputMap({this.node21611932303615});

  NodeOutputMap.fromJson(Map<String, dynamic> json) {
    node21611932303615 = json['node_2_1611932303615'] != null
        ? new Node21611932303615.fromJson(json['node_2_1611932303615'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.node21611932303615 != null) {
      data['node_2_1611932303615'] = this.node21611932303615.toJson();
    }
    return data;
  }
}

class Node21611932303615 {
  List<int> l0;
  List<int> l2;

  Node21611932303615({this.l0, this.l2});

  Node21611932303615.fromJson(Map<String, dynamic> json) {
    l0 = json['0'].cast<int>();
    l2 = json['2'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['0'] = this.l0;
    data['2'] = this.l2;
    return data;
  }
}
