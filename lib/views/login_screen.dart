import 'package:flutter/material.dart';
import 'package:chatbot_pormade/widgets/custom_icon_button.dart';
import 'package:chatbot_pormade/widgets/custom_text_field.dart';

import '../models/alerta.dart';
import '../models/login_api.dart';
import 'chat_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _ctrlLogin = TextEditingController();
  final _ctrlSenha = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          alignment: Alignment.center,
          margin: const EdgeInsets.all(32),
          child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
              ),
              elevation: 16,
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    CustomTextField(
                      hint: 'E-mail',
                      prefix: Icon(Icons.account_circle),
                      textInputType: TextInputType.emailAddress,
                      controller: _ctrlLogin,
                      onChanged: (email) {},
                      enabled: true,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    CustomTextField(
                      hint: 'Senha',
                      prefix: Icon(Icons.lock),
                      obscure: true,
                      controller: _ctrlSenha,
                      onChanged: (pass) {},
                      enabled: true,
                      suffix: CustomIconButton(
                        radius: 32,
                        iconData: Icons.visibility,
                        onTap: () {},
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    SizedBox(
                      height: 44,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(32),
                        ),
                        child: Text('Login'),
                        color: Theme.of(context).primaryColor,
                        disabledColor:
                            Theme.of(context).primaryColor.withAlpha(100),
                        textColor: Colors.white,
                        onPressed: () {
                          _clickButton(context);
                        },
                      ),
                    )
                  ],
                ),
              )),
        ),
      ),
    );
  }

  _clickButton(BuildContext context) async {

    String login = _ctrlLogin.text;
    String senha = _ctrlSenha.text;

    print("login : $login senha: $senha");

    var usuario = await LoginApi.login(login, senha);

    if (usuario != null) {
      print("==> $usuario");

      _navegaHomepage(context);
    } else {
      alert(context, "Login Inválido");
      _ctrlLogin.clear();
      _ctrlSenha.clear();
    }
  }

  _navegaHomepage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ChatScreen()),
    );
  }
}
