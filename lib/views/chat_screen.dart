import 'dart:convert';

import 'package:chatbot_pormade/models/chat_message.dart';
import 'package:chatbot_pormade/widgets/chat_message_list.dart';
import 'package:flutter/material.dart';
import 'package:chatbot_pormade/models/chat_api.dart';
import 'package:chatbot_pormade/models/chat.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreen createState() => _ChatScreen();
}

class _ChatScreen extends State<ChatScreen> {
  final _messageList = <ChatMessage>[];
  final _controllerText = new TextEditingController();
  var chatapi = new List<Generic>();

  @override
  void dispose() {
    //super.dispose();
    _controllerText.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text('Prof. Pormade'),
      ),
      body: Column(
        children: <Widget>[
          _buildList(),
          Divider(height: 1.0),
          _buildUserInput(),
        ],
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildList() {
    return Flexible(
      child: ListView.builder(
        padding: EdgeInsets.all(8.0),
        reverse: true,
        itemBuilder: (_, int index) =>
            ChatMessageListItem(chatMessage: _messageList[index]),
        itemCount: _messageList.length,
      ),
    );
  }

  void _sendMessage({String text, ChatMessageType type}) {
    _controllerText.clear();
    if(type == ChatMessageType.sent){
      _addMessage(name: 'Eu', text: text, type: type);
    }else{
      _addMessage(name: 'Prof. Pormade', text: text, type: type);
    }

  }

  void _addMessage({String name, String text, ChatMessageType type}) {
    var message = ChatMessage(text: text, name: name, type: type);
    setState(() {
      _messageList.insert(0, message);
    });
  }

  Widget _buildTextField() {
    return new Flexible(
      child: new TextField(
        controller: _controllerText,
        decoration: new InputDecoration.collapsed(
          hintText: "Enviar mensagem",
        ),
      ),
    );
  }

  Widget _buildSendButton() {
    return new Container(
      margin: new EdgeInsets.only(left: 8.0),
      child: new IconButton(
          icon: new Icon(Icons.send, color: Colors.lightGreen),
          onPressed: () {
            _clickButton(context);
          }),
    );
  }

  Widget _buildUserInput() {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: new Row(
        children: <Widget>[
          _buildTextField(),
          _buildSendButton(),
        ],
      ),
    );
  }

/* Future _messageReceived(){

    int items = Output().generic.length;

    for(Output){
      var message = Generic().text;

      _addMessage(
          name: 'Professor',
          text: message,
          type: ChatMessageType.received);
    }

}*/

  _clickButton(BuildContext context) async{
    if (_controllerText.text.isNotEmpty) {
      String text = _controllerText.text;

      _sendMessage(text: _controllerText.text, type: ChatMessageType.sent);

      var chat = await ChatApi.chat(text).then((value){
        value.output.generic.forEach((element) {
          if (element.responseType == "text")
          _sendMessage(text: element.text, type: ChatMessageType.received);
        });
      });
    }
  }
}
